package com.kittaphat.robot;

public class Map {
    private int width;
    private int height;
    private Unit units[];
    private int uniCount;
    public Map(int width, int height) {
        this.width = width;
        this.height = height;
        this.units = new Unit[width*height];
        this.uniCount = 0;
    }
    public Map(){
        this(10, 10);
    }
    public void print() {
        for(int y=0; y < this.height; y++) {
            for(int x=0; x < this.width; x++){
                printBlock(x, y);
            }
            System.out.println();
        }
    }

    private void printBlock(int x, int y) {
        for(int i=0; i<uniCount; i++) {
            Unit unit = this.units[i];
            if(unit.isOn(x, y)) {
                System.out.print(unit.getSymbol());
                return;
            }
        }
        System.out.print("-");
    }

    public String toString() {
        return "Map(" + this.width + "," + this.height + ")";
    }

    public void add(Unit unit) {
        if(uniCount==(width*height)) return;
        this.units[uniCount] = unit;
        uniCount++;
    }

    public void printUnits() {
        for(int i=0; i<uniCount; i++) {
            System.out.println(this.units[i]);
        }
    }

    public boolean isOn(int x, int y) {
        return isInWidth(x) && isInHeight(y);
    }

    public boolean isInWidth(int x) {
        return x>=0 && x < width;
    }

    public boolean isInHeight(int y) {
        return y>=0 && y < height;
    }

    public boolean hasDominate(int x, int y) {
        for(int i=0; i<uniCount; i++) {
            Unit unit = this.units[i];
            if(unit.isDominate() && unit.isOn(x, y)) {
                return true;
            }
        }
        return false;
    }
}
